import { Request, Response } from "express";
import {menuObject} from '../helpers/CreateObject'
import {pet} from '../models/Pets';
export const home  = (req: Request, res: Response)=>{
    res.render('pages/page',{
        "menu": menuObject(''),
        "banner":{
            "title": "Todos os animais",
            "background": "allanimals.jpg"
        },
        "list" : pet.getAll()
    })
}

export const dogs  = (req: Request, res: Response)=>{
    res.render('pages/page',{
        "menu":  menuObject('dogs'),
        "banner":{
            "title": "Cachorros",
            "background": "banner_dog.jpg"
        },
        "list": pet.getFromType('dog')
    })
}
export const cats  = (req: Request, res: Response)=>{
    res.render('pages/page',{
        "menu":menuObject('cats'),
        "banner":{
            "title": "Gatos",
            "background": "banner_cat.jpg"
        },
        "list": pet.getFromType('cat')
    })
}
export const fishes  = (req: Request, res: Response)=>{
    res.render('pages/page',{
        "menu":menuObject('fishes'),
        "banner":{
            "title": "Peixes",
            "background": "banner_fish.jpg"
        },
        "list": pet.getFromType('fish')
    })
}