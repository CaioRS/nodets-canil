import { Router } from "express";
import * as PageControllers from '../controllers/pageController'
import * as searchControllers from '../controllers/searchControoler'
const router = Router();

router.get('/', PageControllers.home);
router.get('/dogs', PageControllers.dogs);
router.get('/cats', PageControllers.cats);
router.get('/fishes', PageControllers.fishes);
router.get('/search', searchControllers.search);

export default router