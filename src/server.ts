import express from 'express'
import dotenv from 'dotenv'
import mustache from 'mustache-express'
import path from 'path'
import mainRoutes from './routes/index'
dotenv.config();

const server = express();

//configuração do temple engine
server.set('view engine', 'mustache');
server.set('views', path.join(__dirname, 'views'));
server.engine('mustache', mustache());

//configuração de pasta publica
server.use(express.static(path.join(__dirname, '../public')));


//rotas
server.use(mainRoutes);

//rota para pagina não encontrada
server.use((req,res)=>{
    res.send("pagina não encontrada");
});


server.listen(process.env.PORT);
